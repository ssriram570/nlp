## README

export TF_BINARY_URL=https://storage.googleapis.com/tensorflow/linux/cpu/tensorflow-0.9.0rc0-cp35-cp35m-linux_x86_64.whl

python3 -m pip install $TF_BINARY_URL

pip3 install --force-reinstall --upgrade protobuf

Then at line 82 replace this - losses = tf.nn.softmax_cross_entropy_with_logits(self.scores, self.input_y)
with
losses = tf.nn.softmax_cross_entropy_with_logits(labels = self.input_y, logits = self.scores)

python3 train.py ./data/train.csv.zip ./training_config.json
python3 predict.py ./trained_results_1478563595/ ./data/small_samples.csv
