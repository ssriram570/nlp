from tensorflow.contrib import learn
from collections import Counter
import logging
import os
import re
import json
import itertools
import numpy as np
import pandas as pd
import re

logging.getLogger().setLevel(logging.INFO)

train_file = './data/tagged_plots_movielens.csv.zip'

def keyword_extraction(text):
	"""
	@added
	@text: string

	returns keywords from a sentence after processing it
	"""
	# removing all characters except alphabets
	text = re.sub('[^A-Za-z ]+', '', text)

	# removing stopwords
	stop = set(stopwords.words('english'))
	text = ' '.join([i for i in text.lower().split() if i not in stop])

	# stemming
	stemmer = PorterStemmer()
	text = ' '.join([stemmer.stem(word) for word in text.split()])

	# keyword extraction
	pos_tags = ['NN', 'NNS', 'NNP', 'NNPS', 'RB', 'FW', 'JJ', 'JJR', 'JJS', 'MD', 'POS', \
					'PRP', 'VB', 'VBZ', 'VBP', 'VBD', 'VBN', 'VBG', 'NP', 'PP', 'VP', \
					'ADJP', 'PRT']
	text = ' '.join(keywords(text, ratio=0.7, pos_filter=pos_tags).split('\n'))

	return text

def clean_str(s):
	"""
	@added
	@s: string

	returns cleaned string
	"""
	return s.strip().lower()

def load_embeddings(vocabulary):
	"""
	@modified
	@vocabulary: list of all words

	returns a dict of random initial embeddings of words in vocabulary
	"""
	embeds = {}
	for word in vocabulary:
		embeds[word] = np.random.uniform(-0.2, 0.2, 300)
	#print(embeds)	
	return embeds

def build_vocab(sentences):
	word_counts = Counter(itertools.chain(*sentences))
	vocabulary_inv = [word[0] for word in word_counts.most_common()]
	vocabulary = {word: index for index, word in enumerate(vocabulary_inv)}
	#print(vocabulary)
	return vocabulary, vocabulary_inv

def batch_iter(data, batch_size, num_epochs, shuffle=True):
	data = np.array(data)
	data_size = len(data)
	num_batches_per_epoch = int(data_size / batch_size) + 1

	for epoch in range(num_epochs):
		if shuffle:
			shuffle_indices = np.random.permutation(np.arange(data_size))
			shuffled_data = data[shuffle_indices]
		else:
			shuffled_data = data

		for batch_num in range(num_batches_per_epoch):
			start_index = batch_num * batch_size
			end_index = min((batch_num + 1) * batch_size, data_size)
			yield shuffled_data[start_index:end_index]

def pad_sentences(sentences, padding_word="<PAD/>", forced_sequence_length=None):
	"""Pad setences during training or prediction"""
	if forced_sequence_length is None: # Train
		sequence_length = max(len(x) for x in sentences)
	else: # Prediction
		logging.critical('This is prediction, reading the trained sequence length')
		sequence_length = forced_sequence_length
	logging.critical('The maximum length is {}'.format(sequence_length))

	pad_sentences = []
	for i in range(len(sentences)):
		sentence = sentences[i]
		num_padding = sequence_length - len(sentence)

		if num_padding < 0: # Removing sentences which are not fit as the per the set size
			print('Removing sentence which are longer than trained sequence length')
			pad_sentence = sentence[0:sequence_length]
		else:
			pad_sentence = sentence + [padding_word] * num_padding
		pad_sentences.append(pad_sentence)
	#print(pad_sentence)	
	return pad_sentences

def load_data(filename):
	"""
	@modified 
	@filename: input file containing plot and tag

	returns dataframe of plot, tag, vocabulary and a numpy array of mapped plots and tags
	"""
	df = pd.read_csv(filename, sep='\t')
	selected = ['tag', 'plot']
	non_selected = list(set(df.columns) - set(selected))

	df = df.drop(non_selected, axis=1)
	df = df.dropna(axis=0, how='any', subset=selected)
	df = df.reindex(np.random.permutation(df.index))
	#print(df)
	labels_set = set(df[selected[0]].tolist())
	labels = sorted(list(labels_set))
	num_labels = len(labels)
	one_hot = np.zeros((num_labels, num_labels), int)

	np.fill_diagonal(one_hot, 1)
	label_dict = dict(zip(labels, one_hot))

	x_raw = df[selected[1]].apply(lambda x: clean_str(x).split(' ')).tolist()
	y_raw = df[selected[0]].apply(lambda y: label_dict[y]).tolist()

	#print('Before padding:', x_raw, y_raw)
	x_raw = pad_sentences(x_raw)
	#print('After padding:', x_raw)
	vocabulary, vocabulary_inv = build_vocab(x_raw)
	#print(vocabulary, vocabulary_inv)
	x = np.array([[vocabulary[word] for word in sentence] for sentence in x_raw])
	y = np.array(y_raw)
	#print("Loaded Data!")
	return x, y, vocabulary, vocabulary_inv, df, labels

'''if __name__ == "__main__":	
	print (load_data(train_file))'''
