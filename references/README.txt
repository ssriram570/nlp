README
======

Dependencies and Running Instructions
=====================================
The following packages need to be installed:
python3, numpy, json, pandas, tensorflow v0.9, sklearn, pickle, numpy

To train the model,
python3 train.py /path_to_train_data /path_to_training_config

To test the model,
python3 predict.py /trained_result_dir /path_to_test_data

Code Details
============
1. calc_score.py
This file is used to calculate the F1 score, Precision and Recall using 
the predicted genres and actual genres stored in the output file created
by predict.py

2. data_helper.py
This file contains methods to extract keywords from the movie plots, methods
to extract the vocabulary from the data, initialize embeddings for words in
the vocabulary, load data, and pad the sentences in the data.

3. extract.py
This file contains a method to merge the data extracted from different data sources.
It combines maps the plots in one file to genres in another file and writes it to
a single tsv file containing plots with their genres. It also contains a method to
pre-process the data removing all irrelevant characters from the plots and prepares the
data that's the input to our RNN.

4. generate_arff.py
Used this file to convert our data (consisting 11 classes) into arff format required by Weka.
We used Weka to evaluate our baseline SVM multi-class classifier.

5. predict.py
This file contains a method to load test data, load trained parameters (embeddings, 
words and their indexes, labels). It loads data in sizes of 'batch_size' and creates
the prediction labels for the test data. Then the plot, predicted label and actual label
are written to a file to later compute f1 score by 'calc_score.py'.

6. text_rnn.py
This file is used to build the RNN. It initialized the model with all default parameters.
It computes the weights, biases, hidden layer outputs, creates the LSTM/GRU cells and
assigns losses.

7. train.py
This file contains methods to trains the model, learn the embeddings, learn the parameters of the model,
do cross-validation test, initialize an RNN instance and uses it to build the model.
Finally, it dumps the best model to a file.

8. training_config.json
This contains the configuration parameters.

Note: In all methods of the above files we have specified two tags as follows:
1. @added - this indicates that we added this function to our project.
2. @modified - this indicates we that used existing code, libraries and modified
   a portion of those functions according to our requirements.
3. The untagged functions were not modified by us.

Team Members
============
1. Amit Gupta		110900982
2. Farhaan Jalia	110896810
3. Sriram Sundar	110921718