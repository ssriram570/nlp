from nltk.corpus	import stopwords
from nltk.tokenize 	import word_tokenize
from data_helper	import keyword_extraction

import csv
import re
import nltk

DATA_PATH = '/home/ssriram570/Desktop/projects/nlp_project/nlp/data/MovieSummaries/'
tags = ['Action', 'Adventure', 'Comedy', 'Crime', \
			'Drama', 'Fantasy', 'Horror', 'Mystery', 'Romance Film', 'Science Fiction', 'Thriller']

def merge_data():
	"""
	@added

	function to merge the plot and genre from two different data files
	"""
	d = {}
	with open(DATA_PATH + 'plot_summaries.txt') as f:
		data = csv.reader(f, delimiter='\t')
		for row in data:
			d[row[0]] = row[1]


	outfile = open(DATA_PATH + 'movie_tags.tsv', 'w')
	with open(DATA_PATH + 'movie.metadata.tsv') as f:
		data = csv.reader(f, delimiter='\t')
		for row in data:
			if row[0] in d:
				outfile.write(row[0] + "\t" + row[8] + "\t" + d[row[0]] + "\n")

	outfile.close()


def processing():
	"""
	@added

	function to pre-process the plot and genre
	"""
	stop_words = set(stopwords.words('english'))
	outfile = open(DATA_PATH + 'movie_tags_cmu.tsv', 'w')
	writer = csv.writer(outfile, delimiter='\t')
	writer.writerow(['plot', 'tag'])
	with open(DATA_PATH + 'movie_tags.tsv') as f:
		data = csv.reader(f, delimiter='\t')
		for row in data:
			genres = re.sub('[^A-Za-z ]+', '', row[1]).split()
			new_genres = []
			for genre in genres:
				if genre in tags and genre not in new_genres:
					new_genres.append(genre)

			try:
				new_sentence = keyword_extraction(row[2])
				if len(new_genres) > 0:
					tag = new_genres[0]
					writer.writerow([new_sentence.encode('utf-8'), tag])
			except:
				continue

	outfile.close()


if __name__ == '__main__':
	merge_data()
	processing()
