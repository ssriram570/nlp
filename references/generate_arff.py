import string
PATH="/home/amit/workspace/nlp_project/nlp/data/MovieSummaries/movie_tags_cmu.tsv"

"""
module to generate arff file to evaluate our SVM baseline model in Weka
"""

def parse_data():
	"""
	@added

	returns all tokens in the file
	"""
	with open(PATH) as f:
		all_tokens=[]
		for line in f:
			plot,tag=line.split('\t')
			for word in plot:
				if word not in all_tokens:
					all_tokens.append(word)
	return all_tokens

def create_feature_vectors():
	"""
	@modified from assignment 3

	returns extracted feature vectors
	"""
	with open(PATH) as f:
		all_tokens=[]
		data=[]
		feature_vectors=[]
		for line in f:
			plot,tag=line.split('\t')
			for word in plot.split():
				if word not in all_tokens:
					all_tokens.append(word.lower())	
			data.append(line)
		
		for line in data:
			plot,tag=line.split('\t')
			tag=tag.replace('\n','')
			feature_vector= [0 for t in all_tokens]
			for word in plot.split():
				index=all_tokens.index(word.lower())
				feature_vector[index]+=1
			feature_vector.append(tag)
			feature_vectors.append(feature_vector)
	return feature_vectors,all_tokens
			
def generate_arff_file(feature_vectors, all_tokens, out_path):
	"""
	@modified from assignment 3

	Converts a list of feature vectors to an ARFF file for use with Weka.
	"""
	with open(out_path, 'w') as f:
		# Header info
		f.write("@RELATION tags\n")
		for i in range(len(all_tokens)):
			f.write("@ATTRIBUTE token_{} INTEGER\n".format(i))

		### SPECIFY ADDITIONAL FEATURES HERE ###
		# For example: f.write("@ATTRIBUTE custom_1 REAL\n")

		# Classes
		f.write("@ATTRIBUTE class {Action,Adventure,comedy,Crime,Drama,Fantasy,Horror,Mystery,Romantic Film,Science Fiction,thriller}\n")

		# Data instances
		f.write("\n@DATA\n")
		for fv in feature_vectors:
			features = []
			for i in range(len(fv)):
				value = fv[i]
				if value != 0:
					features.append("{} {}".format(i, value))
			entry = ",".join(features)
			f.write("{" + entry + "}\n")

if __name__=="__main__":
	#data,all_token= parse_data()
	feature_vectors,all_tokens=create_feature_vectors()
	generate_arff_file(feature_vectors,all_tokens,"movie_tag.arff")
