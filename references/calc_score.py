from sklearn.metrics import f1_score
from sklearn.metrics import precision_score, recall_score
from sys import argv
"""
@added

module to calculate f1, precision and recall from the test output file
"""
y_true = []
y_pred = []


index = {'Action':1, 'Adventure':2, 'Comedy':3, 'Crime':4, \
	'Drama':5, 'Fantasy':6, 'Horror':7, 'Mystery':8, 'Romance Film':9, 'Science Fiction':10, 'Thriller':11}

f = open(argv[1])
for line in f:
	line = line.strip('\n')
	pred, plot, true = line.split('|')
	y_true.append(index[true])
	y_pred.append(index[pred])

print (precision_score(y_true, y_pred, average='macro'), recall_score(y_true, y_pred, average='macro'), f1_score(y_true, y_pred, average='macro'))
