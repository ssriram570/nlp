=== Run information ===

Scheme:       weka.classifiers.functions.LibSVM -S 0 -K 2 -D 3 -G 0.0 -R 0.0 -N 0.5 -M 40.0 -C 1.0 -E 0.001 -P 0.1 -model /home/amit/workspace/NLP/weka-3-9-1 -seed 1
Relation:     tags
Instances:    9998
Attributes:   54249
              [list of attributes omitted]
Test mode:    split 80.0% train, remainder test

=== Classifier model (full training set) ===

LibSVM wrapper, original code by Yasser EL-Manzalawy (= WLSVM)

Time taken to build model: 99.64 seconds

=== Evaluation on test split ===

Time taken to test model on test split: 32.4 seconds

=== Summary ===

Correctly Classified Instances         700               35      %
Incorrectly Classified Instances      1300               65      %
Kappa statistic                          0     
Mean absolute error                      0.1182
Root mean squared error                  0.3438
Relative absolute error                 81.9407 %
Root relative squared error            127.6415 %
Total Number of Instances             2000     

=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0.000    0.000    0.000      0.000    0.000      0.000    0.500     0.070     Action
                 0.000    0.000    0.000      0.000    0.000      0.000    0.500     0.039     Adventure
                 0.000    0.000    0.000      0.000    0.000      0.000    0.500     0.187     Comedy
                 0.000    0.000    0.000      0.000    0.000      0.000    0.500     0.092     Crime
                 1.000    1.000    0.350      1.000    0.519      0.000    0.500     0.350     Drama
                 0.000    0.000    0.000      0.000    0.000      0.000    0.500     0.031     Fantasy
                 0.000    0.000    0.000      0.000    0.000      0.000    0.500     0.065     Horror
                 0.000    0.000    0.000      0.000    0.000      0.000    0.500     0.009     Mystery
                 0.000    0.000    0.000      0.000    0.000      0.000    ?         ?         RomanticFilm
                 0.000    0.000    0.000      0.000    0.000      0.000    ?         ?         ScienceFiction
                 0.000    0.000    0.000      0.000    0.000      0.000    0.500     0.158     Thriller
Weighted Avg.    0.350    0.350    0.122      0.350    0.181      0.000    0.500     0.202     

=== Confusion Matrix ===

   a   b   c   d   e   f   g   h   i   j   k   <-- classified as
   0   0   0   0 140   0   0   0   0   0   0 |   a = Action
   0   0   0   0  78   0   0   0   0   0   0 |   b = Adventure
   0   0   0   0 374   0   0   0   0   0   0 |   c = Comedy
   0   0   0   0 184   0   0   0   0   0   0 |   d = Crime
   0   0   0   0 700   0   0   0   0   0   0 |   e = Drama
   0   0   0   0  62   0   0   0   0   0   0 |   f = Fantasy
   0   0   0   0 130   0   0   0   0   0   0 |   g = Horror
   0   0   0   0  17   0   0   0   0   0   0 |   h = Mystery
   0   0   0   0   0   0   0   0   0   0   0 |   i = RomanticFilm
   0   0   0   0   0   0   0   0   0   0   0 |   j = ScienceFiction
   0   0   0   0 315   0   0   0   0   0   0 |   k = Thriller


Test mode:    split 75.0% train, remainder test

=== Classifier model (full training set) ===

LibSVM wrapper, original code by Yasser EL-Manzalawy (= WLSVM)

Time taken to build model: 85.27 seconds

=== Evaluation on test split ===

Time taken to test model on test split: 18.43 seconds

=== Summary ===

Correctly Classified Instances         873               34.934  %
Incorrectly Classified Instances      1626               65.066  %
Kappa statistic                          0     
Mean absolute error                      0.1183
Root mean squared error                  0.344 
Relative absolute error                 82.0634 %
Root relative squared error            127.7267 %
Total Number of Instances             2499     

=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0.000    0.000    0.000      0.000    0.000      0.000    0.500     0.070     Action
                 0.000    0.000    0.000      0.000    0.000      0.000    0.500     0.037     Adventure
                 0.000    0.000    0.000      0.000    0.000      0.000    0.500     0.187     Comedy
                 0.000    0.000    0.000      0.000    0.000      0.000    0.500     0.086     Crime
                 1.000    1.000    0.349      1.000    0.518      0.000    0.500     0.349     Drama
                 0.000    0.000    0.000      0.000    0.000      0.000    0.500     0.032     Fantasy
                 0.000    0.000    0.000      0.000    0.000      0.000    0.500     0.067     Horror
                 0.000    0.000    0.000      0.000    0.000      0.000    0.500     0.008     Mystery
                 0.000    0.000    0.000      0.000    0.000      0.000    ?         ?         RomanticFilm
                 0.000    0.000    0.000      0.000    0.000      0.000    ?         ?         ScienceFiction
                 0.000    0.000    0.000      0.000    0.000      0.000    0.500     0.162     Thriller
Weighted Avg.    0.349    0.349    0.122      0.349    0.181      0.000    0.500     0.203     

=== Confusion Matrix ===

   a   b   c   d   e   f   g   h   i   j   k   <-- classified as
   0   0   0   0 176   0   0   0   0   0   0 |   a = Action
   0   0   0   0  92   0   0   0   0   0   0 |   b = Adventure
   0   0   0   0 468   0   0   0   0   0   0 |   c = Comedy
   0   0   0   0 216   0   0   0   0   0   0 |   d = Crime
   0   0   0   0 873   0   0   0   0   0   0 |   e = Drama
   0   0   0   0  81   0   0   0   0   0   0 |   f = Fantasy
   0   0   0   0 167   0   0   0   0   0   0 |   g = Horror
   0   0   0   0  20   0   0   0   0   0   0 |   h = Mystery
   0   0   0   0   0   0   0   0   0   0   0 |   i = RomanticFilm
   0   0   0   0   0   0   0   0   0   0   0 |   j = ScienceFiction
   0   0   0   0 406   0   0   0   0   0   0 |   k = Thriller